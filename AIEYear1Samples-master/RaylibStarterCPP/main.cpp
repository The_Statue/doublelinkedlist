/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"
#include <iostream>

#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raygui.h"
#include "DLList.h"

DLList* first = new DLList;
DLList* current = first;
DLList* last = current;
DLList* temp = last;

int listCounter = 0;
int listCounterCapped = 0;
int listIndex = 1;
Rectangle backRec = { 0,0,0,0 };
Rectangle titleRec = { 0,0,0,0 };
Rectangle itemRec = { 0,0,0,0 };
Rectangle listRec = { 0,0,0,0 };
Rectangle listItemRec = { 0,0,0,0 };
Rectangle listButtonRec;
Rectangle controlButtonRec;
Rectangle deleteButtonRec;
Rectangle sortButtonRec;


void Delete()
{
	temp = current;

	//How to handle data leak from dereferencing nodes? does garbage collector deal with this? usage minimal enough to ignore?
	if (listIndex != 1 && listIndex != listCounter)
	{
		temp->getPrev()->setNext(temp->getNext());
		temp->getNext()->setPrev(temp->getPrev());

		current = temp->getNext();

	}
	if (listIndex == 1) //Add edge cases for ends of list
	{
		first = first->getNext();
		current = first;
	}
	else if (listIndex == listCounter)
	{
		last = last->getPrev();
		current = last;
		listIndex--;
	}
	listCounter--;
}
void Sort()
{
	int tempVal = 0;
	bool sorted = false;
	while (!sorted) //Loop through the list until fully sorted
	{
		temp = first;
		sorted = true; //assume sorted
		for (int i = 0; i < listCounter - 1; i++)
		{
			tempVal = temp->getNext()->getData();
			if (temp->getData() > tempVal) //Is the current data bigger than the next data, if so, switch
			{
				temp->getNext()->setData(temp->getData());
				temp->setData(tempVal);
				sorted = false; //If a switch was made, assume it will need to be checked again
			}
			temp = temp->getNext();
		}
	}
}
void AddUp()
{
	temp = new DLList;
	temp->setData(listCounter);
	if (listCounter == 0)
	{
		current = temp;
		first = temp;
		last = temp;
	}
	else
	{
		if (listIndex != 1)
		{
			temp->setPrev(current->getPrev());
			current->getPrev()->setNext(temp);
		}
		else
		{
			first = temp;
		}
		temp->setNext(current);
		current->setPrev(temp);
		listIndex++;
	}
	listCounter++;
}
void AddDown()
{
	temp = new DLList;
	temp->setData(listCounter);
	if (listCounter == 0)
	{
		current = temp;
		first = temp;
		last = temp;
	}
	else
	{
		if (listIndex != listCounter)
		{
			temp->setNext(current->getNext());
			current->getNext()->setPrev(temp);
		}
		else
		{
			last = temp;
		}
		temp->setPrev(current);
		current->setNext(temp);
	}
	listCounter++;
}


int main(int argc, char* argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	int screenWidth = 1600;
	int screenHeight = 900;

	backRec = { screenWidth * 0.1f, screenHeight * 0.1f, screenWidth * 0.8f, screenHeight * 0.8f }; //Declare rectangles using screenPeramaters so it scales to screen size
	titleRec = { screenWidth * 0.25f, screenHeight * 0.2f, screenWidth * 0.60f, screenHeight * 0.1f };
	itemRec = { screenWidth * 0.25f, screenHeight * 0.4f, screenWidth * 0.60f, screenHeight * 0.4f };
	listRec = { screenWidth * 0.15f, screenHeight * 0.2f, screenWidth * 0.05f, screenHeight * 0.6f };

	controlButtonRec = { screenWidth * 0.25f, screenHeight * 0.4f, screenWidth * 0.09f, screenHeight * 0.09f };
	deleteButtonRec = { screenWidth * 0.75f, screenHeight * 0.4f, screenWidth * 0.09f, screenHeight * 0.09f };
	sortButtonRec = { screenWidth * 0.75f, screenHeight * 0.5f, screenWidth * 0.09f, screenHeight * 0.09f };


	InitWindow(screenWidth, screenHeight, "Double linked list");
	first->setData(-1);

	SetTargetFPS(60);
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------
		// TODO: Update your variables here
		//----------------------------------------------------------------------------------

		if (listCounter < 20) //Cap the size for items
			listCounterCapped = listCounter;
		else
			listCounterCapped = 19;
		listItemRec = { screenWidth * 0.155f, screenHeight * 0.205f, screenWidth * 0.04f, ((screenHeight * (0.595f / listCounterCapped)) - (screenHeight * 0.005f)) };

		//Buttons for first and last item
		listButtonRec = listItemRec;
		if (CheckCollisionPointRec(GetMousePosition(), listButtonRec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			current = first;
			listIndex = 1;
		}
		listButtonRec.y += (listButtonRec.height + screenHeight * 0.005f) * (listCounterCapped - 1);
		if (CheckCollisionPointRec(GetMousePosition(), listButtonRec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			current = last;
			listIndex = listCounter;
		}

		//Buttons for control
		controlButtonRec.y = screenHeight * 0.4f;
		for (int i = 0; i < 4; i++)//Movement and adding buttons
		{
			if (CheckCollisionPointRec(GetMousePosition(), controlButtonRec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				switch (i) {
				case 0: //Move index up one
					if (listIndex != 1) //Replace checking for nullptr? 
					{
						current = current->getPrev();
						listIndex--;
					}
					break;
				case 1://Add an entry before the current one in the list
					AddUp();
					break;
				case 2://Add an entry after the current one in the list
					AddDown();
					break;
				case 3: //Move index down one
					if (listIndex != listCounter) //Replace checking for nullptr? 
					{
						current = current->getNext();
						listIndex++;
					}
					break;
				}
			}
			controlButtonRec.y += screenHeight * 0.1035f;
		}

		if (CheckCollisionPointRec(GetMousePosition(), deleteButtonRec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && listCounter != 0)
			Delete();

		if (CheckCollisionPointRec(GetMousePosition(), sortButtonRec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && listCounter != 0)
			Sort();

		// Draw
		//----------------------------------------------------------------------------------
		BeginDrawing();

		ClearBackground(RAYWHITE);

		DrawRectangleRec(backRec, GRAY);
		DrawRectangleRec(titleRec, LIGHTGRAY);
		DrawRectangleRec(itemRec, LIGHTGRAY);
		DrawRectangleRec(listRec, LIGHTGRAY);
		DrawText("Double Linked List", titleRec.x + (titleRec.width / 2) - (MeasureText("Double Linked List", screenWidth * 0.035) / 2), titleRec.y + (titleRec.height / 2) - screenWidth * 0.02, screenWidth * 0.035, BLACK);
		if (listCounter != 0)
		{
			for (int i = 0; i < listCounter; i++)
			{
				if (i == listIndex - 1 && (listCounterCapped < 19 || i == 0) || (i == listCounterCapped - 1 && listIndex == listCounter))
					DrawRectangleRec(listItemRec, BLACK);
				else if (i == 0)
				{
					if (CheckCollisionPointRec(GetMousePosition(), listItemRec))
						DrawRectangleRec(listItemRec, GRAY);
					else
						DrawRectangleRec(listItemRec, RED);
				}
				else if (i == listCounterCapped - 1)
				{
					if (CheckCollisionPointRec(GetMousePosition(), listItemRec))
						DrawRectangleRec(listItemRec, GRAY);
					else
						DrawRectangleRec(listItemRec, BLUE);
				}
				else if (i < 19 && listCounterCapped < 19)
					DrawRectangleRec(listItemRec, DARKGRAY);
				listItemRec.y += listItemRec.height + screenHeight * 0.005f;

				if (listCounterCapped >= 19)
				{
					if (i == (int)floor((float)listIndex * (18.0f / (float)listCounter)) - 1) //aproximate the location the user is looking at
						DrawRectangleRec(listItemRec, BLACK);

					if (i == 8)// draws text showing what percentage the user is looking at
						DrawText(TextFormat("%i", listIndex), listItemRec.x + listItemRec.width - (MeasureText(TextFormat("%i", listIndex), screenWidth * 0.02)), listItemRec.y, screenWidth * 0.02, DARKGRAY);
					if (i == 9)
						DrawText(TextFormat("%i", listCounter), listItemRec.x + listItemRec.width - (MeasureText(TextFormat("%i", listCounter), screenWidth * 0.02)), listItemRec.y, screenWidth * 0.02, DARKGRAY);
				}
			}

			DrawText(TextFormat("Current Data: %i", current->getData()), itemRec.x + (itemRec.width / 2) - (MeasureText(TextFormat("Current Data: %i", current->getData()), screenWidth * 0.035) / 2), itemRec.y + (itemRec.height / 2) - screenWidth * 0.035, screenWidth * 0.035, BLACK);
		}

		//DrawButtons for control
		controlButtonRec.y = screenHeight * 0.4f;
		for (int i = 0; i < 4; i++)
		{
			if (CheckCollisionPointRec(GetMousePosition(), controlButtonRec))
				DrawRectangleRec(controlButtonRec, DARKGRAY);
			else
				DrawRectangleRec(controlButtonRec, PURPLE);

			switch (i) {
			case 0:
				DrawText("Up", controlButtonRec.x + (controlButtonRec.width / 2) - (MeasureText("Up", screenWidth * 0.015) / 2), controlButtonRec.y + (controlButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);
				break;
			case 1:
				DrawText("Add-Up", controlButtonRec.x + (controlButtonRec.width / 2) - (MeasureText("Add-Up", screenWidth * 0.015) / 2), controlButtonRec.y + (controlButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);
				break;
			case 2:
				DrawText("Add-Down", controlButtonRec.x + (controlButtonRec.width / 2) - (MeasureText("Add-Down", screenWidth * 0.015) / 2), controlButtonRec.y + (controlButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);
				break;
			case 3:
				DrawText("Down", controlButtonRec.x + (controlButtonRec.width / 2) - (MeasureText("Down", screenWidth * 0.015) / 2), controlButtonRec.y + (controlButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);
				break;
			}

			controlButtonRec.y += screenHeight * 0.1035f;
		}
		//Draw extra buttons
		if (listCounter != 0)
		{
			//Draw delete button
			if (CheckCollisionPointRec(GetMousePosition(), deleteButtonRec))
				DrawRectangleRec(deleteButtonRec, DARKGRAY);
			else
				DrawRectangleRec(deleteButtonRec, RED);

			DrawText("Delete", deleteButtonRec.x + (deleteButtonRec.width / 2) - (MeasureText("Delete", screenWidth * 0.015) / 2), deleteButtonRec.y + (deleteButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);

			//Draw sort button
			if (CheckCollisionPointRec(GetMousePosition(), sortButtonRec))
				DrawRectangleRec(sortButtonRec, DARKGRAY);
			else
				DrawRectangleRec(sortButtonRec, GREEN);

			DrawText("Sort", sortButtonRec.x + (sortButtonRec.width / 2) - (MeasureText("Sort", screenWidth * 0.015) / 2), sortButtonRec.y + (sortButtonRec.height / 2) - screenWidth * 0.005, screenWidth * 0.015, BLACK);
		}
		EndDrawing();
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------   
	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}