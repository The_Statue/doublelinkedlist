#include "DLList.h"
void DLList::setPrev(DLList* prev)
{
	previous = prev;
}
DLList* DLList::getPrev()
{
	return previous;
}
void DLList::setNext(DLList* nxt)
{
	next = nxt;
}
DLList* DLList::getNext()
{
	return next;
}
void DLList::setData(int d)
{
	data = d;
}
int DLList::getData()
{
	return data;
}