#pragma once
struct DLList
{
public:
	void setPrev(DLList* prev);
	DLList* getPrev();
	void setNext(DLList* nxt);
	DLList* getNext();
	void setData(int d);
	int getData();

private:
	DLList* previous;
	DLList* next;
	int data = -1;
};

