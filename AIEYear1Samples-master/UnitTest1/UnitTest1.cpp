#include "pch.h"
#include "CppUnitTest.h"
#include "DLList.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace DLLUnitTest
{
	DLList* first = new DLList;
	DLList* current = first;
	DLList* last = current;
	DLList* temp = last;

	int listCounter = 0;
	int listIndex = 1;

	void Delete()
	{
		temp = current;

		//How to handle data leak from dereferencing nodes? does garbage collector deal with this? usage minimal enough to ignore?
		if (listIndex != 1 && listIndex != listCounter)
		{
			temp->getPrev()->setNext(temp->getNext());
			temp->getNext()->setPrev(temp->getPrev());

			current = temp->getNext();

		}
		if (listIndex == 1) //Add edge cases for ends of list
		{
			first = first->getNext();
			current = first;
		}
		else if (listIndex == listCounter)
		{
			last = last->getPrev();
			current = last;
			listIndex--;
		}
		listCounter--;
	}
	void Sort()
	{
		int tempVal = 0;
		bool sorted = false;
		while (!sorted) //Loop through the list until fully sorted
		{
			temp = first;
			sorted = true; //assume sorted
			for (int i = 0; i < listCounter - 1; i++)
			{
				tempVal = temp->getNext()->getData();
				if (temp->getData() > tempVal) //Is the current data bigger than the next data, if so, switch
				{
					temp->getNext()->setData(temp->getData());
					temp->setData(tempVal);
					sorted = false; //If a switch was made, assume it will need to be checked again
				}
				temp = temp->getNext();
			}
		}
	}
	void First()
	{
		current = first;
		listIndex = 1;
	}
	void Last()
	{
		current = last;
		listIndex = listCounter;
	}
	void Up()
	{
		if (listIndex != 1) //Replace checking for nullptr? 
		{
			current = current->getPrev();
			listIndex--;
		}
	}
	void Down()
	{
		current = current->getNext();
		listIndex++;
	}
	void AddUp()
	{
		temp = new DLList;
		temp->setData(listCounter);
		if (listCounter == 0)
		{
			current = temp;
			first = temp;
			last = temp;
		}
		else
		{
			if (listIndex != 1)
			{
				temp->setPrev(current->getPrev());
				current->getPrev()->setNext(temp);
			}
			else
			{
				first = temp;
			}
			temp->setNext(current);
			current->setPrev(temp);
			listIndex++;
		}
		listCounter++;
	}
	void AddDown()
	{
		temp = new DLList;
		temp->setData(listCounter);
		if (listCounter == 0)
		{
			current = temp;
			first = temp;
			last = temp;
		}
		else
		{
			if (listIndex != listCounter)
			{
				temp->setNext(current->getNext());
				current->getNext()->setPrev(temp);
			}
			else
			{
				last = temp;
			}
			temp->setPrev(current);
			current->setNext(temp);
		}
		listCounter++;
	}


	TEST_CLASS(DLLUnitTest)
	{
	public:

		TEST_METHOD(SortTest1)
		{
			//Addup only then sort
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();

			Sort();

			Assert::AreEqual(16, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			for (int i = 0; i < listCounter; i++)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getNext();
			}
			temp = last;
			for (int i = listCounter-1; i > 0; i--)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getPrev();
			}
		}
		TEST_METHOD(SortTest2)
		{
			//AddDown only then sort
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();
			AddDown();

			Sort();

			Assert::AreEqual(16, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			for (int i = 0; i < listCounter; i++)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getNext();
			}
			temp = last;
			for (int i = listCounter - 1; i > 0; i--)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getPrev();
			}
		}
		TEST_METHOD(SortTest3)
		{
			//Alternate AddUp and AddDown then sort
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();

			Sort();

			Assert::AreEqual(16, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			for (int i = 0; i < listCounter; i++)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getNext();
			}
			temp = last;
			for (int i = listCounter - 1; i > 0; i--)
			{
				Assert::AreEqual(i, temp->getData());
				temp = temp->getPrev();
			}
		}

		TEST_METHOD(DeleteTest1) {
			//Delete every second item, from back to front.
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();

			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();
			Up();
			Up();
			Delete();

			Sort();

			Assert::AreEqual(8, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			for (int i = 0; i < listCounter; i++)
			{
				Assert::AreEqual(i*2, temp->getData());
				temp = temp->getNext();
			}
			temp = last;
			for (int i = listCounter - 1; i > 0; i--)
			{
				Assert::AreEqual(i*2, temp->getData());
				temp = temp->getPrev();
			}
		}
		TEST_METHOD(DeleteTest2) {
			//Delete every second item, from front to back.
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();
			AddUp();

			First();

			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();
			Delete();
			Down();

			Sort();

			Assert::AreEqual(8, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			for (int i = 0; i < listCounter; i++)
			{
				Assert::AreEqual(i*2, temp->getData());
				temp = temp->getNext();
			}
			temp = last;
			for (int i = listCounter - 1; i > 0; i--)
			{
				Assert::AreEqual(i*2, temp->getData());
				temp = temp->getPrev();
			}
		}
		TEST_METHOD(DeleteTest3) {
			//Add items alternating from up and down, go to start of list and delete every third item.
			first = new DLList;
			current = first;
			last = current;
			temp = last;

			listCounter = 0;
			listIndex = 1;
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();
			AddUp();
			AddDown();

			First();

			Delete();
			Down();
			Down();
			Delete();
			Down();
			Down();
			Delete();
			Down();
			Down();
			Delete();
			Down();
			Down();
			Delete();
			Down();
			Down();
			Delete();
			Down();
			Down();

			Sort();

			Assert::AreEqual(10, listCounter); // Check if correct amount of items / counted correctly

			temp = first;
			Assert::AreEqual(0, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(3, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(4, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(5, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(6, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(9, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(10, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(11, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(12, temp->getData());
			temp = temp->getNext();
			Assert::AreEqual(15, temp->getData());
			temp = temp->getNext();
		}
	};
}
